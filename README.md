# API using Laravel with Oracle DB


## Installation

Dengan menggunakan terminal, dapatkan code di gitlab
	
	git clone https://gitlab.com/zam3858/orcltrain.git

Masuk ke dalam folder
	
	cd orcltrain
	
Copy sample config file (.env.example) 
	
	cp .env.example .env

Edit .env dengan setting yang betul

Generate key untuk kegunaan encryption sistem

	php artisan key:generate

Dapatkan library-library projek

	composer install

Run database migration jika perlu

	php artisan migrate


## Reference

https://gitlab.com/zam3858/orcltrain

https://laracasts.com/series/php-for-beginners

https://laracasts.com/series/laravel-6-from-scratch

### Tools

Insomnia

https://insomnia.rest/download

## Installing OCI8 using laragon

	- Download instant client ( http://www.oracle.com/technetwork/topics/winx64soft-089540.html )
	- Extract all the dll in the downloaded zip file to PHP dir (Menu > PHP > Version > dir:php-xxx-xxx)
	- Just click Menu > PHP > Extensions > php_oci8_12c.dll
	- reload Apache
	- check phpinfo();


## Untuk laravel menggunakan oci8, library yajra/laravel-oci8 digunakan:

Jika belum, tambahkan library ini menggunakan composer:

	composer require yajra/laravel-oci8

Arahan dibawah akan meletakkan setting oracle ke config/oracle.php

	php artisan vendor:publish --tag=oracle


## Usage

Laravel Eloquent model can be used most of the time except for some difference

### Rujukan Query mengikut kegunaan

#### DB Query builder:

	https://laravel.com/docs/6.x/queries

#### Eloquent (model Laravel):

	https://laravel.com/docs/6.x/eloquent

Untuk rujukan query melibatkan relationship
	
	https://laravel.com/docs/6.x/eloquent-relationships


### Note menggunakan yajra/laravel-oci8

#### BLOB and CLOB

To use BLOB and CLOB, model inherit the Eloquent class provided by yajra instead

	use yajra\Oci8\Eloquent\OracleEloquent as Model;

	class Person extends Model {
	    protected $binaries = ['image', 'bio'];
	}


#### Sequence

the default sequence for the table is table_column_seq but can be overidden
using the $sequence = '' model attribute 

	class Person extends Model {
	    public $table = 'people';
	    public $sequence = 'sq_people';
	}

	$person = new Person();
	$person->name = 'Marvin, the Paranoid Android';
	$person->save();


#### Executing Procedures


	$pdo = DB::getPdo();
	$p1 = 8;

	$stmt = $pdo->prepare("begin myproc(:p1, :p2); end;");
	$stmt->bindParam(':p1', $p1, PDO::PARAM_INT);
	$stmt->bindParam(':p2', $p2, PDO::PARAM_INT);
	$stmt->execute();

	return $p2; // prints 16



## API

### Persediaan 

API Token boleh digunakan untuk login untuk itu kemaskini table users dengan field api_token

Create migration to add token to users table: 
	
Di terminal

	php artisan make:migration addApiTokenToUsers --table=users

Dan edit file migration yang jana sebentar tadi

	Schema::table('users', function ($table) {
	    $table->string('api_token', 80)->after('password')
	                        ->unique()
	                        ->nullable()
	                        ->default(null);
	});


Untuk memanggil stored procedure

	public function darabdua() {
        /**
        CREATE OR REPLACE PROCEDURE myproc(p1 IN NUMBER, p2 OUT NUMBER) AS
        BEGIN
            p2 := p1 * 2;
        END;
        /
         */

        $pdo = \DB::getPdo();
        $p1 = 8;

        $stmt = $pdo->prepare("begin myproc(:p1, 
                                            :p2); 
                                            end;");
        $stmt->bindParam(':p1', $p1, \PDO::PARAM_INT);
        $stmt->bindParam(':p2', $p2, \PDO::PARAM_INT);
        $stmt->execute();

        return $p2; // prints 16

    }

### Mewajibkan token untuk akses 

Di route/api.php

	
	Route::middleware('auth:api')->group(function() {
		//semua routes di dalam block ini wajib diakses menggunakan token
		Route::get('users', 'UserController@index');
	});

Secara default, jika tiada token laravel akan pergi ke route 'login' jadi kita createkan satu route untuk handle error apabila tiada token. Masih dalam route/api.php

	Route::get('login', function() {
	 return response()->json(
            [
                'errors' => [
                    'status' => 401,
                    'message' => 'Unauthenticated',
                ]
            ], 401
        );
	})->name('login');

### Akses menggunakan token

#### Menggunakan url query string

	http://oraclembpj.test/users?api_token=tokenDijanaUntukPengguna;

#### Dimasukkan sebagai data dalam form POST

Untuk post, masukkan "api_token" beserta nilai token sebagai salah satu data dihantar.


