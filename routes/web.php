<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//http://oraclembpj.test
Route::get('/', function () {
    return view('welcome');
});

//http://oraclembpj.test/users
// Route::get('/users', function() {

// 	return App\User::get();

// } );

Route::get('/users/{user}', function($user) {
	return App\User::where('id','=', $user)->first();

	return App\User::find($user);
});

// Route::get('/users/{name}/{email}', function($name, $email) {
// 	$users = App\User::where('name', $name)
// 						->orWhere('email','LIKE', $email ."%")
// 						->first();

// 	return $users;
// });

//http://oraclembpj.test/pengguna
Route::get('pengguna', 'UserController@index');

Route::get('reset_token/{user}', 'UserController@reset_token');

//http://oraclembpj.test/pengguna/1/admin@admin.com
// atau http://oraclembpj.test/pengguna/1/
Route::get('pengguna/{user}/{email?}', 'UserController@show');

//http://oraclembpj.test/pengguna?q=search&year_register=2019

Route::get('users/reset_token/{user}', 'UserController@reset_token');

Route::get('oracle1', 'UserController@darabdua');

Route::get('oracle2', 'UserController@dengan_cursor');