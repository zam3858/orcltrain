<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dump(request()->q);
        // //dump and die = dd();
        // dd( request()->year_register );

        return User::with('rooms')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $email=null)
    {
        return User::where('id', $id)->firstOrFail();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    //NOTE: bukan untuk production. bagi production
    // gunakan current maklumat user yang login
    public function reset_token($user_id)
    {

        $token = \Str::random(80);

        User::findOrFail($user_id)->forceFill([
            'api_token' => hash('sha256', $token),
        ])->save();

        return ['token' => $token];
    }

    public function postuser() {
        dd(\Auth::user());
        return request()->all();
    }

    public function darabdua() {
        /**
        CREATE OR REPLACE PROCEDURE myproc(p1 IN NUMBER, p2 OUT NUMBER) AS
        BEGIN
            p2 := p1 * 2;
        END;
        /
         */

        $pdo = \DB::getPdo();
        $p1 = 8;

        $stmt = $pdo->prepare("begin myproc(:p1, 
                                            :p2); 
                                            end;");
        $stmt->bindParam(':p1', $p1, \PDO::PARAM_INT);
        $stmt->bindParam(':p2', $p2, \PDO::PARAM_INT);
        $stmt->execute();

        return $p2; // prints 16

    }


    public function dengan_cursor() {
    // PHP call Oracle stored procedure with an in, out parameter

    /**

    CREATE OR REPLACE PROCEDURE get_user_by_alpha(user_alpha IN VARCHAR, usr_result OUT SYS_REFCURSOR)
    IS BEGIN
        OPEN usr_result FOR
            SELECT name, email
            FROM users
            WHERE name LIKE CONCAT(user_alpha,'%');
    END;
    /

    */
        $query = 'a';

        return \DB::transaction(function($conn) use ($query) {

        $pdo = $conn->getPdo();

        $sql = "begin get_user_by_alpha(:user_alpha, :usr_result); end;";
        $stmt = $pdo->prepare($sql);

        $stmt->bindParam(':user_alpha', $query, \PDO::PARAM_STR);
        $stmt->bindParam(':usr_result', $result, \PDO::PARAM_STMT);

        $stmt->execute();

        oci_execute($result, OCI_DEFAULT);
        oci_fetch_all($result, $array, 0, -1, OCI_FETCHSTATEMENT_BY_ROW + OCI_ASSOC );
        oci_free_cursor($result);

        if(empty($array)) {
            return "no result";
        } else {
            return $array;    
        }
        

        });
    }

    public function api_login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (\Auth::attempt($credentials)) {
             $token = \Str::random(80);
            \Auth::user()->forceFill([
                'api_token' => hash('sha256', $token),
            ])->save();
            return [ 'api_token' => $token, 'user'=> \Auth::user() ];
        }
    }

}
