<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'rooms';
    protected $primaryKey = 'id';
    protected $keyType = 'string';

    function manager() {
    	return $this->belongsTo('App\User',
    							'user_id');
    }

}
