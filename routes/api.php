<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::any('login', function() {
	 // return response()->json(
  //           [
  //               'errors' => [
  //                   'status' => 401,
  //                   'message' => 'Unauthenticated',
  //               ]
  //           ], 401
  //       );
    abort(401);
})->name('login');


Route::middleware('auth:api')->group(function() {
	//semua routes di dalam block ini wajib diakses menggunakan token
	// Route::post('users', 'UserController@postuser');
 //    Route::get('/users', 'UserController@index');

    Route::get('rooms/{room}/byId', 'RoomController@showById');
    Route::resource('rooms', 'RoomController');
});

Route::post('api_login', 'UserController@api_login');

//Route::get('/users', 'UserController@index');

//http://oraclembpj.test/api/rooms
//POST
//